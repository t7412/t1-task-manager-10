package ru.t1.chubarov.tm.api;

import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskService  extends ITaskRepository{

    Task create(String name, String description);
}
