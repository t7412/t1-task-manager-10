package ru.t1.chubarov.tm.api;

import ru.t1.chubarov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create (String name, String description);
}
