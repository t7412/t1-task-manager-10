package ru.t1.chubarov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
