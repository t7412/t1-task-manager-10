package ru.t1.chubarov.tm.api;

import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    void add (Task task);

    void clear();

    List<Task> findAll();
}
