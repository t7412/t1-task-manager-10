package ru.t1.chubarov.tm.api;

public interface IProjectController {
    void createProject();

    void showProjects();

    void clearProjects();
}
