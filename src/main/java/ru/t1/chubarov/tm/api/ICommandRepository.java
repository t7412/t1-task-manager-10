package ru.t1.chubarov.tm.api;

import ru.t1.chubarov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
