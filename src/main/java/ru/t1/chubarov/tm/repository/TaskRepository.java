package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.ITaskRepository;
import ru.t1.chubarov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task){
        tasks.add(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll(){
        return tasks;
    }

}
